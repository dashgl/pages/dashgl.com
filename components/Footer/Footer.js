import { Container, Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import Link from "next/link";
import React from "react";
import footerStyles from "../../styles/Footer.module.css";
import YouTubeIcon from "@mui/icons-material/YouTube";
import TwitterIcon from "@mui/icons-material/Twitter";
import GitHubIcon from "@mui/icons-material/GitHub";
import RedditIcon from "@mui/icons-material/Reddit";
import { BsMastodon } from "react-icons/bs";
import { AiOutlineGitlab } from "react-icons/ai";

const Footer = () => {
  return (
    <Container className={footerStyles.footer} sx={{ py: 6 }}>
      <Grid
        container
        spacing={{ xs: 2, md: 3, lg: 4 }}
        columns={{ xs: 1, sm: 8, md: 12, lg: 12 }}
      >
        <Grid item xs={1} sm={8} md={3} lg={3}>
          <Box sx={{ textAlign: { xs: "center", md: "left" } }}>
            <img src='/images/logo-2.png' alt="DashGL Logo Footer Banner" />
          </Box>
        </Grid>

        <Grid item xs={1} sm={2} md={2} lg={2}>
          <Box sx={{ textAlign: { xs: "center", sm: "left" } }}>
            <Typography sx={{ fontSize: 24, mb: 1 }} variant="h6">
              Project
            </Typography>
            <Link href="https://github.com/kion-dgl">Tutorials</Link>
            <Link href="https://gitlab.com/dashgl/format">Model Format</Link>
            <Link href="https://gitlab.com/dashgl/">Online Tools</Link>
          </Box>
        </Grid>

        <Grid item xs={1} sm={2} md={2} lg={2}>
          <Box sx={{ textAlign: { xs: "center", sm: "left" } }}>
            <Typography sx={{ fontSize: 24, mb: 1 }} variant="h6">
              Information
            </Typography>
            <Link href="/about">About</Link>
            <Link href="/contribute">Contribute</Link>
            <Link href="/support">Support</Link>
          </Box>
        </Grid>

        <Grid item xs={1} sm={2} md={2} lg={2}>
          <Box sx={{ textAlign: { xs: "center", sm: "left" } }}>
            <Typography sx={{ fontSize: 24, mb: 1 }} variant="h6">
              Links
            </Typography>
            <Link href="https://blog.dashgl.com/">blog.dashgl.com</Link>
            <Link href="https://gtk.dashgl.com/">gtk.dashgl.com</Link>
            <Link href="http://egl.dashgl.com/">egl.dashgl.com</Link>
          </Box>
        </Grid>

        <Grid item xs={1} sm={2} md={3} lg={3}>
          <Box sx={{ textAlign: { xs: "center", sm: "left" } }}>
            <Typography sx={{ fontSize: 24, mb: 1 }} variant="h6">
              Social links
            </Typography>
            {/* social icons  */}
            <Box>
              <Box
                sx={{
                  display: "flex",
                  gap: 2,
                  justifyContent: { xs: "center", sm: "left" },
                }}
              >
                <a
                  href="https://www.youtube.com/channel/UCGGYpxGASdAOmSp36lhTiIw"
                  target="_blank"
                  rel="noreferrer"
                >
                  <YouTubeIcon className={footerStyles.filedIcon} />
                </a>
                <a href="https://twitter.com/kion_dgl" target="_blank" rel="noreferrer">
                  <TwitterIcon className={footerStyles.emptyIcon} />
                </a>
                <a href="https://github.com/kion-dgl" target="_blank" rel="noreferrer">
                  <GitHubIcon className={footerStyles.filedIcon} />
                </a>
              </Box>

              {/*  */}
              <Box
                sx={{
                  display: "flex",
                  gap: 2,
                  justifyContent: { xs: "center", sm: "left" },
                }}
              >
                <a
                  href="https://linuxrocks.online/@kion"
                  target="_blank"
                  rel="noreferrer"
                >
                  <BsMastodon className={footerStyles.emptyIcon} />
                </a>
                <a
                  href="https://www.reddit.com/user/kion_dgl/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <RedditIcon className={footerStyles.filedIcon} />
                </a>
                <a
                  href="https://gitlab.com/dashgl"
                  target="_blank"
                  rel="noreferrer"
                >
                  <AiOutlineGitlab className={footerStyles.emptyIcon} />
                </a>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
      <Box sx={{ pt: 2, mt: 2, borderTop: "1px solid #ddd" }}>
        <Typography sx={{ textAlign: "center" }} variant="body2">
          Copyright &copy; 2017 - <span>{new Date().getFullYear()}</span> DashGL
        </Typography>
      </Box>
    </Container>
  );
};

export default Footer;
