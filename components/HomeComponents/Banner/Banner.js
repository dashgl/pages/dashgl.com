import { Box, Button, Container, Grid, Typography } from "@mui/material";
import React from "react";
import homeStyles from "../../../styles/Home.module.css";
import Navigation from "../../Navigation/Navigation";

const Banner = () => {
  return (
    <Box className={homeStyles.banner}>
      {/* Navigation  */}
      <Navigation />
      {/* banner  */}
      <Container sx={{ py: 12 }}>
        <Grid item
          sx={{
            alignItems: "center",
            px: 4,
            mb: 10,
          }}
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 1, md: 12 }}
        >
          <Grid item xs={1} md={7}>
            <Box>
              <img
                  className={homeStyles.bannerLogo}
                  src='/images/logo_white.png'
                  alt=""
                  width={400}
                />
              <Typography variant="subtitle1" sx={{ color: "#fff", my: 2 }}>
                DashGL is a site devoted to resources for learning hardware
                accerlated graphics using C on Linux. The site offers a number
                of project-based tutorials where you start from a simple
                Hello-World example and build up to a working project.
              </Typography>
              <Button
                variant=""
                className="dashglBtn"
                sx={{ boxShadow: 1, mt: 3 }}
              >
                Get Started
              </Button>
            </Box>
          </Grid>
          <Grid item xs={1} md={5}>
            <Box sx={{ textAlign: { xs: "center", md: "center" } }}>
              <img src='/images/tyson-tan-full.png' alt="" className={homeStyles.splashImage} />
            </Box>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default Banner;
