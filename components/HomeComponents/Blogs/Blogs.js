import { Container, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import SwiperCore, { Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.min.css";
import "swiper/swiper.min.css";
import SingleBlog from "./SingleBlog";
// install Swiper modules
SwiperCore.use([Navigation]);

const blogs = [
  {
    id: "1",
    blogTitle: "More Productive Working Flow.",
    description:
      "I Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pulvinar et ante etiam iaculis enim enim odio semper tellus. Congue feugiat consectetur donec non,",
    author: "Admin",
    date: "17 July, 2021",
    thumb: '/images/blog-1.jpg',
  },
  {
    id: "2",
    blogTitle: "More Productive Working Flow.",
    description:
      "I Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pulvinar et ante etiam iaculis enim enim odio semper tellus. Congue feugiat consectetur donec non,",
    author: "Admin",
    date: "17 July, 2021",
    thumb: '/images/blog-2.jpg',
  },
  {
    id: "3",
    blogTitle: "More Productive Working Flow.",
    description:
      "I Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pulvinar et ante etiam iaculis enim enim odio semper tellus. Congue feugiat consectetur donec non,",
    author: "Admin",
    date: "17 July, 2021",
    thumb: '/images/blog-3.jpg',
  },
  {
    id: "4",
    blogTitle: "More Productive Working Flow.",
    description:
      "I Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pulvinar et ante etiam iaculis enim enim odio semper tellus. Congue feugiat consectetur donec non,",
    author: "Admin",
    date: "17 July, 2021",
    thumb: '/images/blog-4.jpg',
  },
];

const Blogs = () => {
  return (
    <Box sx={{ bgcolor: "#F3FAFC" }}>
      <Container sx={{ py: 6 }}>
        <Typography
          variant="h4"
          sx={{ fontSize: 36, my: 3, fontWeight: 700, textAlign: "center" }}
        >
          From the Blog
        </Typography>
        {/* blogs  */}
        <Swiper
          style={{ padding: 40 }}
          navigation={true}
          grabCursor={true}
          autoplay={{
            delay: 2000,
            disableOnInteraction: false,
          }}
          className="mySwiper"
          slidesPerView={1}
          spaceBetween={10}
          breakpoints={{
            640: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            768: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            1024: {
              slidesPerView: 3,
              spaceBetween: 30,
            },
          }}
        >
          {blogs.map((blog) => (
            <SwiperSlide key={blog.id}>
              <SingleBlog blog={blog} />
            </SwiperSlide>
          ))}
        </Swiper>
      </Container>
    </Box>
  );
};

export default Blogs;
