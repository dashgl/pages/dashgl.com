import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import React from "react";
import homeStyles from "../../../styles/Home.module.css";
import PersonIcon from "@mui/icons-material/Person";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";
import ArrowRightAltIcon from "@mui/icons-material/ArrowRightAlt";

const SingleBlog = ({ blog }) => {
  const { blogTitle, description, thumb, date, author } = blog;
  return (
    <Box>
      <Card className={homeStyles.blogCard}>
        <Box sx={{ textAlign: "center" }}>
          <CardMedia component="img" alt={blogTitle} image={thumb} />
        </Box>
        <CardContent sx={{ p: 1 }}>
          <Box
            sx={{
              display: { sm: "flex" },
              justifyContent: "space-between",
              alignItems: "center",
              mb: 2,
            }}
          >
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                gap: 1,
                color: "#0095D4",
              }}
            >
              <span>
                <PersonIcon />
              </span>
              <span>{author}</span>
            </Box>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                gap: 1,
                color: "#0095D4",
              }}
            >
              <span>
                <CalendarTodayIcon />
              </span>
              <span>{date}</span>
            </Box>
          </Box>
          <Typography
            sx={{ fontSize: 18 }}
            gutterBottom
            variant="h5"
            component="div"
          >
            {blogTitle}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Lizards are a widespread group of squamate reptiles, with over 6,000
            species, ranging across all continents except Antarctica
          </Typography>
        </CardContent>
        <CardActions>
          <Button endIcon={<ArrowRightAltIcon />} size="small">
            Read More
          </Button>
        </CardActions>
      </Card>
    </Box>
  );
};

export default SingleBlog;
