import {
  Box,
  Button,
  Container,
  Divider,
  Paper,
  Typography,
} from "@mui/material";
import React from "react";
import homeStyles from "../../../styles/Home.module.css";

const Feature = () => {
  return (
    <Box>
      <Container sx={{ py: 4 }}>
        <Paper
          className={homeStyles.feature}
          sx={{ display: { sm: "flex" }, gap: 4, alignItems: "center" }}
        >
          <Box sx={{ textAlign: "center" }}>
            <img
              src="/images/dashie-2d.png"
              alt=""
              className={homeStyles.freeModel}
            />
          </Box>
          <Box>
            <Typography variant="h5" gutterBottom>
              Rigged Live 2D Model
            </Typography>
            <Divider color="primary" sx={{ width: 40 }} />
            <Typography variant="body2" color="text.secondary" sx={{ my: 3 }}>
              Live 2d model Of Dashie is free to use under CC0 1.0 Universal
              Download it and use it for whatever. No attribution required.
              <p>
                Model created by:
                <a href="https://twitter.com/DEIVICHII">@DEIVICHII</a>
              </p>
              <p>
                Model rigged by:
                <a href="https://twitter.com/Sophielpaka">@Sophielpaka</a>
              </p>
            </Typography>
            <Button
              href="/2d_dashie.rar"
              sx={{ borderRadius: 4 }}
              variant="contained"
            >
              Download
            </Button>
          </Box>
        </Paper>

        <Paper
          className={homeStyles.feature}
          sx={{ display: { sm: "flex" }, mt: 2, gap: 4, alignItems: "center" }}
        >
          <Box sx={{ textAlign: "center" }}>
            <img
              src="/images/free-mood.png"
              alt=""
              className={homeStyles.freeModel}
            />
          </Box>
          <Box>
            <Typography variant="h5" gutterBottom>
              Rigged 3d model Model
            </Typography>
            <Divider color="primary" sx={{ width: 40 }} />
            <Typography variant="body2" color="text.secondary" sx={{ my: 3 }}>
              Model Of Dashie is free to use under CC0 1.0 Universal Download it
              and use it for whatever. No attribution required.
              <p>
                Model Created by:
                <a href="https://vidavic.artstation.com/">vidavic</a>
              </p>
            </Typography>
            <Button
              href="/dashie_by_vidavic.rar"
              sx={{ borderRadius: 4 }}
              variant="contained"
            >
              Download
            </Button>
          </Box>
        </Paper>
      </Container>
    </Box>
  );
};

export default Feature;
