import { Box, Container, Grid, Typography } from "@mui/material";
import React from "react";
import SingleTutorial from "./SingleTutorial";
import homeStyles from "../../../styles/Home.module.css";

const tutorials = [
  {
    id: "1",
    title: "Brickout",
    date: "Sept. 15, 2017",
    description:
      "This tutorial covers how to make a Brickout Clone using the GTK+ library for a window context. OpenGL for rendering, and DashGL to manage matrix manipulation. We'll start off with something very simple, a very primitive clone of Brickout that doesn't use any sprites. We'll learn how to declare vertices and uniforms to draw basic objects on the screen and declare keydown callbacks to move entities on the screen.",
    features: [
      "Language: C",
      "Window Library: GTK",
      "Graphics Library: OpenGL 3.0",
      "Target Platform: Raspberry Pi",
    ],
    img: "/images/tutorial-1.jpg",
    repository: "https://github.com/kion-dgl/DashGL-GTK-Brickout-Tutorial",
    url: "https://gtk.dashgl.com/Brickout/",
  },
  {
    id: "2",
    title: "Astroids",
    date: "Nov. 12, 2017",
    description:
      "Simple Astroids clone using assets from Kenney's Space Shooter Redux and Space Shooter Extension. The pace of this tutorial will be slightly faster than previous tutorials. As concepts that were introduced one at a time before will now be grouped into a single step. This tutorial will not be drastically different from the Invaders tutorial. We will mainly focus on movement, direction and acceleration. And how to draw the same sprite multiple times to create a wrapping effect. ",
    features: [
      "Language: C",
      "Window Library: GTK",
      "Graphics Library: OpenGL 3.0",
      "Target Platform: Raspberry Pi",
    ],
    img: "/images/tutorial-2.jpg",
    repository: "https://github.com/kion-dgl/DashGL-GTK-Astroids-Tutorial",
    url: "https://gtk.dashgl.com/Astroids/",
  },
  {
    id: "3",
    title: "Invaders",
    date: "Oct. 8, 2017",
    description:
      "Simple Invaders clone using Kenney's Space Shooter redux pack. In this tutorial we will include sprites to make a more complete looking game, but it will still be fairly simple in nature. We have a group of enemies that fire at the player while moving down the screen and a player that can fire back. We use static arrays to create a very simple bullet management system. And use the pythagorean theorem for basic hit detection. ",
    features: [
      "Language: C",
      "Window Library: GTK",
      "Graphics Library: OpenGL 3.0",
      "Target Platform: Raspberry Pi",
    ],
    img: "/images/tutorial-3.jpg",
    repository: "https://github.com/kion-dgl/DashGL-GTK-Invaders-Tutorial",
    url: "https://gtk.dashgl.com/Invaders/",
  },
];

const LatestTutorials = () => {
  return (
    <Box className={homeStyles.tutorialsSection}>
      <Container sx={{ py: 6 }}>
        <Typography variant="h4" sx={{ fontSize: 36, my: 3, fontWeight: 700 }}>
          Latest Tutorials
        </Typography>
        {/* shapes  */}
        <Box className={homeStyles.shape1}>
          <img src="/images/shape-1.png" alt="" width={80} height={80} />
        </Box>
        <Box className={homeStyles.shape2}>
          <img src="/images/shape-2.png" alt="" width={120} height={120} />
        </Box>
        {/* shapes  */}
        {tutorials.map((tutorial) => (
          <SingleTutorial key={tutorial.id} tutorial={tutorial} />
        ))}
        {/* shapes  */}
        <Box className={homeStyles.shape3}>
          <img src="/images/shape-3.png" alt="" width={150} height={150} />
        </Box>
      </Container>
    </Box>
  );
};

export default LatestTutorials;
