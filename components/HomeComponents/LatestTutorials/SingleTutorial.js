import { Divider, Grid, Paper, Typography, Button } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import homeStyles from "../../../styles/Home.module.css";
import { styled } from "@mui/material/styles";
import { makeStyles } from "@mui/styles";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

const useStyles = makeStyles({
  paper: {
    width: "100%",
  },
});

const SingleTutorial = ({ tutorial }) => {
  const classes = useStyles();
  const { title, date, description, features, img } = tutorial;

  return (
    <Paper className={homeStyles.tutorialBox} sx={{ flexGrow: 1 }}>
      <Box
        className={homeStyles.tutorialInnerBox}
        sx={{ display: { sm: "flex" }, gap: 4 }}
      >
        <Grid container spacing={{ xs: 0, sm: 2, md: 4 }}>
          <Grid item xs={12} md={4}>
            <img className={homeStyles.tutorialImg} src={img} alt="" />
          </Grid>
          <Grid item xs={12} md={8} sx={{ p: 2 }}>
            <Box sx={{ display: "flex", alignItems: "center", pt: 2 }}>
              <Typography sx={{ pr: 1, mr: 1 }} variant="h5">
                {title}
              </Typography>
              <Typography
                sx={{ marginTop: "3px", color: "#2F9B01" }}
                variant="subtitle2"
              >
                {date}
              </Typography>
            </Box>
            <Box sx={{ my: 2 }}>
              <Typography variant="body1">{description}</Typography>
              <Button
                variant=""
                className="dashglBtn"
                sx={{ boxShadow: 1, mt: 1, width: { xs: "100%", sm: "30%" } }}
                href={tutorial.url}
              >
                Get Started
              </Button>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Paper>
  );
};

export default SingleTutorial;
