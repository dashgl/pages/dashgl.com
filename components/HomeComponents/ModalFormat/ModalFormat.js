import {
  Box,
  Button,
  Container,
  Divider,
  Grid,
  Paper,
  Typography,
} from "@mui/material";
import React from "react";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import homeStyles from "../../../styles/Home.module.css";

const ModalFormat = () => {

  return (
    <Box sx={{ pt: 1, pb: 8 }}>
      <Container sx={{ pt: 1, pb: 8 }}>
        <Typography
          variant="h4"
          sx={{
            fontSize: 36,
            my: 4,
            pb: 5,
            fontWeight: 700,
            textAlign: "center",
          }}
        >
          Model Format
        </Typography>
        <Grid
          container
          // spacing={{ xs: 2, md: 0, lg: 0 }}
          columns={{ xs: 1, sm: 2, md: 3, lg: 12 }}
          sx={{ position: "relative" }}
        >
          {/* 1st modalBox  */}
          <Grid item xs={1} sm={1} md={1} lg={4}>
            <Paper
              className={`${homeStyles.modalBox} ${homeStyles.firstModalFormat}`}
              sx={{ p: 4 }}
            >
              <Typography
                sx={{ fontSize: 26 }}
                color="primary"
                variant="h5"
                gutterBottom
              >
                Noesis
              </Typography>
              <Divider color="primary" sx={{ width: 40, borderWidth : 1.5, borderColor: '#18a403' }} />
              {/* import  */}
              <Box sx={{ my: 2 }}>
                <Typography sx={{ fontSize: 21 }} variant="h6" gutterBottom>
                  Import
                </Typography>
                <Divider />
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Mesh
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Textures
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Armature
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Animations
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
              </Box>
              {/* import  */}
              {/* <Divider color="primary" sx={{ width: 40 }} /> */}
              {/* Export  */}
              <Box sx={{ my: 2 }}>
                <Typography sx={{ fontSize: 21 }} variant="h6" gutterBottom>
                  Export
                </Typography>
                <Divider />
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Mesh
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Textures
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Armature
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Animations
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
              </Box>
              {/* import  */}
              <Box sx={{ textAlign: "center" }}>
                <Button
                  className={homeStyles.modalBtn}
                  sx={{ borderRadius: 4, px: 4, py: 1 }}
                  variant="contained"
                  href='https://gitlab.com/dashgl/format/-/tree/master/plugin/noesis'
                >
                  Download Plugin
                </Button>
              </Box>
            </Paper>
          </Grid>
          {/* 1st modalBox  */}
          {/* 2nd modal box  */}
          <Grid item xs={1} sm={1} md={1} lg={4}>
            <Paper
              className={`${homeStyles.modalBox} ${homeStyles.secondModalFormat}`}
              sx={{ p: 4 }}
            >
              <Typography
                sx={{ fontSize: 26 }}
                color="primary"
                variant="h5"
                gutterBottom
              >
                Threejs
              </Typography>
              <Divider color="primary" sx={{ width: 40, borderWidth : 1.5, borderColor: '#18a403' }} />
              {/* import  */}
              <Box sx={{ my: 2 }}>
                <Typography sx={{ fontSize: 21 }} variant="h6" gutterBottom>
                  Import
                </Typography>
                <Divider />
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Mesh
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Textures
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Armature
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Animations
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
              </Box>
              {/* import  */}
              {/* Export  */}
              <Box sx={{ my: 2 }}>
                <Typography sx={{ fontSize: 21 }} variant="h6" gutterBottom>
                  Export
                </Typography>
                <Divider />
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Mesh
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Textures
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Armature
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Animations
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
              </Box>
              {/* import  */}
              <Box sx={{ textAlign: "center" }}>
                <Button
                  className={homeStyles.modalBtn}
                  sx={{ borderRadius: 4, px: 4, py: 1 }}
                  variant="contained"
                  href='https://gitlab.com/dashgl/format/-/tree/master/plugin/threejs'
                >
                  Download Plugin
                </Button>
              </Box>
            </Paper>
          </Grid>
          {/* 2nd modal box  */}
          {/* 3rd modalBox  */}
          <Grid item xs={1} sm={1} md={1} lg={4}>
            <Paper
              className={`${homeStyles.modalBox} ${homeStyles.thirdModalFormat}`}
              sx={{ p: 4 }}
            >
              <Typography
                sx={{ fontSize: 26 }}
                color="primary"
                variant="h5"
                gutterBottom
              >
                Blender
              </Typography>
              <Divider color="primary" sx={{ width: 40, borderWidth : 1.5, borderColor: '#18a403' }} />
              {/* import  */}
              <Box sx={{ my: 2 }}>
                <Typography sx={{ fontSize: 21 }} variant="h6" gutterBottom>
                  Import
                </Typography>
                <Divider />
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Mesh
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Textures
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <CheckCircleIcon color="primary" />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Armature
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <img
                      className={homeStyles.closeIcon}
                      src='/images/close_white_24dp.svg'
                      alt=""
                      width={20}
                      height={20}
                    />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Animations
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
              </Box>
              {/* import  */}
              {/* Export  */}
              <Box sx={{ my: 2 }}>
                <Typography sx={{ fontSize: 21 }} variant="h6" gutterBottom>
                  Export
                </Typography>
                <Divider />
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <img
                      className={homeStyles.closeIcon}
                      src='/images/close_white_24dp.svg'
                      alt=""
                      width={20}
                      height={20}
                    />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Mesh
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <img
                      className={homeStyles.closeIcon}
                      src='/images/close_white_24dp.svg'
                      alt=""
                      width={20}
                      height={20}
                    />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Textures
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <img
                      className={homeStyles.closeIcon}
                      src='/images/close_white_24dp.svg'
                      alt=""
                      width={20}
                      height={20}
                    />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Armature
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
                {/* single */}
                <Box sx={{ my: 2 }}>
                  <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
                    <img
                      className={homeStyles.closeIcon}
                      src='/images/close_white_24dp.svg'
                      alt=""
                      width={20}
                      height={20}
                    />
                    <Typography sx={{ color: "#848199" }} variant="subtitle1">
                      Animations
                    </Typography>
                  </Box>
                </Box>
                {/* single */}
              </Box>
              {/* import  */}
              <Box sx={{ textAlign: "center" }}>
                <Button
                  className={homeStyles.modalBtn}
                  sx={{ borderRadius: 4, px: 4, py: 1 }}
                  variant="contained"
                  href='https://gitlab.com/dashgl/format/-/tree/master/plugin/blender'
                >
                  Download Plugin
                </Button>
              </Box>
            </Paper>
          </Grid>
          {/* 3rd modal box  */}
        </Grid>
      </Container>
    </Box>
  );
};

export default ModalFormat;
