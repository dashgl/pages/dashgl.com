import { CssBaseline } from "@mui/material";
import React from "react";
import Footer from "../Footer/Footer";
import Navigation from "../Navigation/Navigation";

const Layout = ({ children }) => {
  return (
    <>
      {/* main content */}
      <main>
        <CssBaseline>{children}</CssBaseline>
      </main>
      {/* footer  */}
      <Footer />
    </>
  );
};

export default Layout;
