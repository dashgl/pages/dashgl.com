import { ThemeProvider } from "@emotion/react";
import "@fontsource/poppins/400.css";
import "@fontsource/poppins/500.css";
import "@fontsource/poppins/700.css";
import { createTheme } from "@mui/material";
import Header from "../components/Header/Header";
import Layout from "../components/Layout";
import "../styles/globals.css";

// customize theme colors

const theme = createTheme({
  palette: {
    primary: {
      main: "#18A403",
    },
    secondary: {
      main: "#312869",
    },
  },
});

function MyApp({ Component, pageProps }) {
  return (
    <>
      {/* website head  */}
      <Header />
      {/* layout  */}
      <Layout>
        <ThemeProvider theme={theme}>
          <Component {...pageProps} />
        </ThemeProvider>
      </Layout>
    </>
  );
}

export default MyApp;
