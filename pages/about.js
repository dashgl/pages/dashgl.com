import React from "react";
import { Typography, Container } from "@mui/material";

const index = () => {
  return (
    <Container>
        <Typography variant='h3'>About</Typography>

        <Typography variant='body1' gutterBottom>
            DashGL started a project to read and export 3d files from 
            early 3d generation games, as a way to try and understand
            the fundamental concepts of how 3d works in computer graphics.
        </Typography>

        <Typography variant='body1' gutterBottom>
            As as extention of that came two other components that became 
            part of the scope of the project. The first of these is Tutorials
            for how to write accelerated hardward graphics in C on the
            Raspberry Pi for Linux. The reason being is to try and use the
            graphics API's directly to see how 3d is rendered in an engine.
        </Typography>

        <Typography variant='body1'>
            The next component is the model format which arose for the 
            lack of an interchangable format between different application.
            While GLTF has made a lot of improvements on that front, the
            model format still exists to provide a simple-as-possible-model
            format for working in other languages and to provide a reference
            implementation for various plugins. 
        </Typography>

        <Typography variant='h3'>Social</Typography>

        <Typography variant='body1'>
             I should also provide a full list of social accounts that are
             used for dashgl.
        </Typography>

        <ul>
          <li>Email</li>
          <li>Github</li>
          <li>Gitlab</li>
          <li>Youtube</li>
          <li>Twitter</li>
          <li>Minds</li>
          <li>Mastodon</li>
          <li>liberapay</li>
          <li>Patreon</li>
          <li>Ko-fi</li>
          <li>Reddit</li>
          <li>Discord</li>
        </ul>

        <Typography variant='h3'>License</Typography>

        <Typography variant='body1'>
          Quick note about license policy for the tutorials and assets used here.
        </Typography>

        <ul>
          <li>Tutorials are released under the Unlicense - effectively public domain</li>
          <li>Libraries are released under the MIT license - permissive license, include header</li>
          <li>Assets are released under creative commons no attribute required</li>
          <li>Tools are released under GPLv3 - free to clone, use and update</li>
        </ul>

    </Container>
  );
};

export default index;
