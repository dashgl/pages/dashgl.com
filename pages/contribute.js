import React from "react";
import { Typography, Container } from "@mui/material";
import Link from 'next/link'

const index = () => {
  return (
    <Container>
        <Typography variant='h3'>Contribute</Typography>

        <Typography variant='body1' gutterBottom>
          I guess this is a list of things that I plan to do anyways
          but wouldn't mind if other people got there first. A quick
          list of way to contribute it the following.
        </Typography>

        <ul>
          <li>Make tutorials for Raspberry Pi/Linux</li>
          <li>Screencast or make youtube tutorials</li>
          <li>Provide art or assets that can be used by others</li>
          <li>Tweet or share the project with your followers</li>
          <li>Implement Dash Model Format plugin for some 3d software</li>
          <li>Support us on one of the platforms in the <Link href='/support'>Support Page</Link></li>
        </ul>

        <Typography variant='h3'>Contributers</Typography>

        <Typography variant='body1' gutterBottom>
          In addition to ways to contribute, i think it would make sense to 
          use this space to give credit to people who have contributed. 
        </Typography>

        <ul>
          <li>Tyson-Tan for the Dashie Mascot: <a href='https://tysontan.com/'>https://tysontan.com/</a></li>
          <li>Vidavic for the Dashie model: <a href='https://vidavic.artstation.com/'>https://vidavic.artstation.com/</a></li>
          <li>Studio King for the Logo - https://twitter.com/brandjahadul</li>
          <li>Andreywebb for the splash page design</li>
          <li>Suhag Al Amin for the splash page<a href='https://github.com/developer-suhag'>https://github.com/developer-suhag</a></li>
        </ul>

    </Container>
  );
};

export default index;
