import { Typography } from "@mui/material";
import React from "react";
import Banner from "../components/HomeComponents/Banner/Banner";
import Blogs from "../components/HomeComponents/Blogs/Blogs";
import Feature from "../components/HomeComponents/Feature/Feature";
import LatestTutorials from "../components/HomeComponents/LatestTutorials/LatestTutorials";
import ModalFormat from "../components/HomeComponents/ModalFormat/ModalFormat";

const index = () => {
  return (
    <div>
      {/* banner section  */}
      <Banner />
      {/* Latest Tutorials */}
      <LatestTutorials />
      {/* blogs  */}
      <Blogs />
      {/* feature  */}
      <Feature />
      {/* modal format  */}
      <ModalFormat />
    </div>
  );
};

export default index;
