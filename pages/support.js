import React from "react";
import { Typography, Container } from "@mui/material";

const index = () => {
  return (
    <Container>
        <Typography variant='h3'>Get Support</Typography>

        <Typography variant='body1' gutterBottom>
          If you need support for something, the best way to get in touch
          is to send me a tweet or an email.
        </Typography>

        <ul>
          <li>kion@dashgl.com</li>
          <li>https://twitter.com/kion_dgl</li>
        </ul>

        <Typography variant='h3'>Support DashGl</Typography>

        <Typography variant='body1'>
          If for some magical reason you want to support DashGL, then you
          can use the support links below. Not a non-profit or anything. 
          Funds will be used to spend more time on the project, or to
          commision assets from artists.
        </Typography>

        <ul>
          <li>liberapay</li>
          <li>Patreon</li>
          <li>Ko-fi</li>
          <li>Bitcoin</li>
          <li>Dogecoin</li>
          <li>Ethereum</li>
        </ul>

    </Container>
  );
};

export default index;
